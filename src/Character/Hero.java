package Character;

import Equipment.*;

import java.util.HashMap;

public abstract class Hero {



    // all Hero's (mage, route, ranger, warrior) have
    // Name, Level, base primary attributes, total primary attributes
    // Base => from its primary attributes and levels (character attributes, level bonus)
    // Total => weapon/equipment bonus

    //Idea => CharacterCLasses with Mage, hunter...

    private final String name;
    private int level, strength, dexterity, intelligence;

    private int basePrimaryAttribute, totalPrimaryAttribute;

    private HashMap<String, Equipment> equipment;

    String[] equitableWeapon;
    String[] equitableArmor;


    public int getPrimaryAttribute(){
        //System.out.println("STR" + strength + " INT" + intelligence + " DEX" + dexterity);
        if (dexterity>strength && dexterity>intelligence) return dexterity;
        else return Math.max(intelligence, strength);
    }
    public void setAttributes(int strength, int dexterity, int intelligence ) {
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    public void levelUpAttributes(int strength, int dexterity, int intelligence ) {
        this.strength += strength;
        this.dexterity += dexterity;
        this.intelligence += intelligence;
    }


    public Hero(String name, int strength, int dexterity, int intelligence, String[] equitableWeapon, String[] equitableArmor) {
        this.name = name;
        setAttributes(strength, dexterity, intelligence);
        this.level = 1;
        basePrimaryAttribute = getPrimaryAttribute();
        initializeEquipment();
        setTotalPrimaryAttribute();
        this.equitableArmor = equitableArmor;
        this.equitableWeapon = equitableWeapon;
    }
    public void initializeEquipment(){
        this.equipment = new HashMap<>();
        this.equipment.put("Head", null);
        this.equipment.put("Body", null);
        this.equipment.put("Legs", null);
        this.equipment.put("Weapon", null);
    }

    public void equipHead(Armor head) {
        if (head == null) {
            System.out.println("Head is broken!");
            return;
        }
        for (String type: equitableArmor) {
            if (type.equalsIgnoreCase(head.getType())) {
                this.equipment.put("Head", head);
                setTotalPrimaryAttribute();
                return;
            }
        }
        this.equipment.put("Head", null);
    }
    public void equipBody(Armor body) {
        if (body == null) {
            System.out.println("Body is broken!");
            return;
        }
        for (String type: equitableArmor) {
            if (type.equalsIgnoreCase(body.getType())) {
                this.equipment.put("Body", body);
                setTotalPrimaryAttribute();
                return;
            }
        }
        this.equipment.put("Body", null);
    }
    public void equipLegs(Armor legs) {
        if (legs == null) {
            System.out.println("Legs is broken!");
            return;
        }
        for (String type: equitableArmor) {
            if (type.equalsIgnoreCase(legs.getType())) {
                this.equipment.put("Legs", legs);
                setTotalPrimaryAttribute();
                return;
            }
        }
        this.equipment.put("Legs", null);
    }
    public void equipWeapon(Weapon weapon) {
        if (weapon == null) {
            System.out.println("You Cannot Equip This!");
            return;
        }
        for (String type: equitableWeapon) {
            if (type.equalsIgnoreCase(weapon.getType())) {
                this.equipment.put("Weapon", weapon);
                setTotalPrimaryAttribute();
                return;
            }
        }
        this.equipment.put("Weapon", null);
    }


    public int equipmentAttributes(){
        int total=0;

        if(manifestArmor("Head") != null) total += manifestArmor("Head").getPrimaryAttribute();
        if(manifestArmor("Legs") != null) total += manifestArmor("Legs").getPrimaryAttribute();
        if(manifestArmor("Body") != null) total += manifestArmor("Body").getPrimaryAttribute();

        return total;
    }

    //Created for enabling DRY in the event of type casting
    public Armor manifestArmor(String armor){
        return (Armor)equipment.get(armor);
    }
    public Weapon manifestWeapon() {
        return (Weapon)equipment.get("Weapon");
    }

    public void levelUp(int strength, int dexterity, int intelligence){
        this.level +=1;
        levelUpAttributes(strength, dexterity, intelligence);
        basePrimaryAttribute = getPrimaryAttribute();
        setTotalPrimaryAttribute();
    }

    public String getName() {
        return this.name;
    }

    public int getTotalPrimaryAttribute(){
        return this.totalPrimaryAttribute;
    }



    public void setTotalPrimaryAttribute(){
        totalPrimaryAttribute = getPrimaryAttribute() + equipmentAttributes();
    }


    // Since dealing with % on integers the returned amount is a double
    // this poses issues later on when dealing with health, since health then cannot be an int without some hacking
    // furthermore this poses the question of i should make the weapon, attackPerSecond and thereby DPS doubles?
    public double calculateDamage() {
        double total =0;
        if (manifestWeapon() == null){
            System.out.println("Your Weapon is Missing!");
        return (1 +(double)getTotalPrimaryAttribute()/100);}
        //System.out.println("DPS:" + manifestWeapon().getDPS() + "TotalPriAT: " + getTotalPrimaryAttribute() + " That but divided by 100 " + (1+(double)getTotalPrimaryAttribute()/100) );
        total = manifestWeapon().getDPS()*(1+(double)getTotalPrimaryAttribute()/100);

        return total;
    };

}


/* To be implemented
* Attributes => Strength, Dexterity, Intelligence (Primary Attributes)
*  1(point) Strength => Warriors damage by 1%
*  1(point) Dexterity => Rangers Rouges damage by 1%
*  1(point) Intelligence => Mages 's damage by 1%
*
* Damage => depends on Weapon equipped, character class and the amount of primary attributes
*   the base damage is determined by weapon? and Character class
*
*
*
*           */


