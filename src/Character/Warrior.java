package Character;

import Equipment.Armor;
import Equipment.Weapon;

public class Warrior extends Hero{

    public Warrior(String name) {
        super(name, 5, 2, 1, equitableWeapon, equitableArmor);
    }

    public void levelUp() {
        super.levelUp(3, 2, 1);
    }


    static String[] equitableWeapon ={
        "Axe",
        "Hammer",
        "Sword"
    };
    static String[] equitableArmor = {
        "Mail",
        "Plate"
    };
}
