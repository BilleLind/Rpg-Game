package Character;

import Equipment.*;

public class Mage extends Hero{

    public Mage(String name) {
        super(name, 1, 1, 8, equitableWeapon, equitableArmor);
    }

    public void levelUp() {
        super.levelUp(1, 1, 5);
    }

    static String[] equitableWeapon = {
            "Staff",
            "Wand"
    };
    static  String[] equitableArmor = {
            "Cloth"
    };
}
