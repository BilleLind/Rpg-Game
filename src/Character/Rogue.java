package Character;

import Equipment.Armor;
import Equipment.Weapon;

public class Rogue extends Hero{

    public Rogue(String name) {
        super(name, 2, 6, 1, equitableWeapon, equitableArmor);
    }

    public void levelUp() {
        super.levelUp(1, 4, 1);
    }


    static String[] equitableWeapon = {
        "Dagger",
        "Sword"
    };
    static String[] equitableArmor = {
        "Leather",
        "Mail"
    };
}
