package Character;

import Equipment.Armor;
import Equipment.Weapon;

public class Ranger extends Hero{

    public Ranger(String name) {
        super(name, 1, 7, 1, equitableWeapon, equitableArmor);
    }

    public void levelUp() {
        super.levelUp(1, 5, 1);
    }



    static String[]  equitableWeapon = {
        "Bow"
    };
    static String[]  equitableArmor ={
        "Leather",
        "Mail"
    };
}
