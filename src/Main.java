import Equipment.*;
import Character.*;

import java.util.Scanner;

import static Equipment.Armor.createArmor;
import static Equipment.Weapon.createWeapon;


public class Main {
    public static void main(String[] args)  {

        // Example usage
        Scanner input = new Scanner(System.in);
        System.out.println("Please choose an character type:");
        System.out.println("1: Mage, 2: Warrior, 3: Ranger, 4: Rouge..");
        int choice = input.nextInt();

        System.out.print("Now input your name: ");
        String name = input.next();


        if (choice == 1) {
            Mage hero = new Mage(name);
            Weapon weapon = createWeapon("starter", 1,2,2,"Wand");
            Armor head = createArmor("Starter", 1, "Head", "Cloth", 1);
            Armor body = createArmor("Starter", 1, "Body", "Cloth", 1);
            Armor legs = createArmor("Starter", 1, "Legs", "Cloth", 1);
            hero.equipWeapon(weapon);
            hero.equipHead(head);
            hero.equipBody(body);
            hero.equipLegs(legs);

            monsterEvent(hero);
            hero.levelUp();
            System.out.println("You have leveled up!");
            System.out.println("Your primary attributes are now: " + hero.getPrimaryAttribute());
            System.out.println("Your Total primary attributes are now: " + hero.getTotalPrimaryAttribute());

            monsterEvent(hero);
            System.out.println("After defeating monster you found a staff!");
            Weapon wand = createWeapon("Legendary Wand", 1,10,2,"Wand");
            hero.equipWeapon(wand);
            System.out.println("your dps have increased " + hero.calculateDamage());

        } else if (choice == 2) {
            Warrior hero = new Warrior(name);
            Weapon weapon = createWeapon("starter", 1,2,2,"Axe");
            Armor head = createArmor("Starter", 1, "Head", "Plate", 1);
            Armor body = createArmor("Starter", 1, "Body", "Plate", 1);
            Armor legs = createArmor("Starter", 1, "Legs", "Plate", 1);
            hero.equipWeapon(weapon);
            hero.equipHead(head);
            hero.equipBody(body);
            hero.equipLegs(legs);

        }else if (choice == 3) {
            Ranger hero = new Ranger(name);
            Weapon weapon = createWeapon("starter", 1,2,2,"Bow");
            Armor head = createArmor("Starter", 1, "Head", "Mail", 1);
            Armor body = createArmor("Starter", 1, "Body", "Mail", 1);
            Armor legs = createArmor("Starter", 1, "Legs", "Mail", 1);
            hero.equipWeapon(weapon);
            hero.equipHead(head);
            hero.equipBody(body);
            hero.equipLegs(legs);

        } else {
            Rogue hero = new Rogue(name);
            Weapon weapon = createWeapon("starter", 1,2,2,"Dagger");
            Armor head = createArmor("Starter", 1, "Head", "Leather", 1);
            Armor body = createArmor("Starter", 1, "Body", "Leather", 1);
            Armor legs = createArmor("Starter", 1, "Legs", "Leather", 1);
            hero.equipWeapon(weapon);
            hero.equipHead(head);
            hero.equipBody(body);
            hero.equipLegs(legs);

        }










    }

    //since the monster event are made to be used by all, overloaded methods like levelup() are not available
    static void monsterEvent(Hero hero){
        System.out.println("A monster appears, deal it damage!");
        double health = 20;

        for (int i = 0; health >0; i++) {
            System.out.println("Damage dealt: " + hero.calculateDamage());
            health =health - hero.calculateDamage();
            System.out.println("Health left: " + health);
        }
    }

}
