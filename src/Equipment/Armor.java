package Equipment;

public class Armor extends Equipment {

    private int primaryAttribute;
    public Armor(String name, int requiredLevel, String slot, String type, int attribute) throws InvalidArmorException {
        super(name, requiredLevel, slot, setArmorType(type));
        //Armor specific
        this.primaryAttribute = attribute;
    }

    public int getPrimaryAttribute() {
        return this.primaryAttribute;
    }


    public static Armor createArmor(String name, int requiredLevel, String slot, String type, int attribute) {
        try{
            return new Armor(name, requiredLevel, slot, type, attribute );
        }catch (InvalidArmorException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    static String setArmorType(String typeToBeChecked) throws InvalidArmorException {
        for (armor_types type: armor_types.values()){
            if (type.toString().equalsIgnoreCase(typeToBeChecked)) {
                return typeToBeChecked;
            }
        }
        throw new InvalidArmorException("Invalid Armor Type!");
    }
    /* TODO
        TotalBaseAttribute point increase
        Type check - pr. class also



    */




    public static class InvalidArmorException extends Exception{
        public InvalidArmorException(String message) {
            super(message);
        }
    }

}
