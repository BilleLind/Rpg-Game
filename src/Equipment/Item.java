package Equipment;

public abstract class Item {

    /*
        Item gives Equipment and there through Weapon and Armor the base(common) data types
        and utility methods to assist in the creation and utilization of Armor and Weapons.
        Enum's for Weapon and Armor is To ensure correctly named Equipment types, check in the respectively classes.
    */
    private String name, slot, type;
    private int requiredLevel;

    public Item(String name, int requiredLevel, String slot, String type) {
        this.name = name;
        this.requiredLevel = requiredLevel;
        this.slot = slot;
        this.type = type;
    }

    public Item() {
    }

    public String getName() {
        if (name == null) return "Name is null";
        return name;
    }

    public int getRequiredLevel() {
        return requiredLevel;
    }


    public String getSlot() {
        return slot;
    }

    public String getType() {
        return type;
    }




    enum weapon_types  {
        Axe,
        Bow,
        Dagger,
        Hammer,
        Staff,
        Sword,
        Wand
    }


    enum armor_types {
        Cloth,
        Leather,
        Mail,
        Plate
    }

}
