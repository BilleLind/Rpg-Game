package Equipment;

public class Weapon extends Equipment {

    /*
        TODO createWeapon => if null create broken weapon or instead of having null use a basic equipment: bare fisted, bandana, scrap t-shirt, scrap legs...
        TODO ReWrite/undue null checks, when basic equipment is implemented.
    */


    private int damage, attackPerSecond;
    public Weapon(String name, int requiredLevel, int damage, int attackPerSecond, String type) throws InvalidWeaponException {
        super(name, requiredLevel, "Weapon", setWeaponType(type));
            setDamage(damage);
            setAttackPerSecond(attackPerSecond);
    }
    static String setWeaponType(String typeToBeChecked) throws InvalidWeaponException {
        for (weapon_types type: weapon_types.values()){
            if (type.toString().equals(typeToBeChecked)) {
                return typeToBeChecked;
            }
        }
        throw new InvalidWeaponException("Invalid Weapon Type!");
    }
    public static Weapon createWeapon(String name, int requiredLevel,  int damage, int attackPerSecond, String type) {
        try{
            return new Weapon(name, requiredLevel, damage, attackPerSecond, type);
        }catch (InvalidWeaponException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public int getAttackPerSecond() {
        return attackPerSecond;
    }

    public void setAttackPerSecond(int attackPerSecond) {
        this.attackPerSecond = attackPerSecond;
    }

    public double getDPS(){
        if (getDamage() > 0) {
            return getDamage() * getAttackPerSecond();
        }
        System.out.println("The Weapon is broken!");
        return 1;
    }

    public static class InvalidWeaponException extends Exception{
        public InvalidWeaponException(String message) {
            super(message);
        }
    }

}
