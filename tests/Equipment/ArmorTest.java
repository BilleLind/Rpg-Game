package Equipment;

import org.junit.jupiter.api.Test;

import Equipment.Armor.InvalidArmorException;
import Equipment.Armor.*;

import java.util.Objects;

import static Equipment.Armor.createArmor;
import static Equipment.Armor.setArmorType;
import static org.junit.jupiter.api.Assertions.*;

class ArmorTest {

    @Test
    void getPrimaryAttribute() {
        int expected = 4;
        Armor head = Armor.createArmor("name", 1, "Head", "Cloth", 4);

        int actual = head.getPrimaryAttribute();

        assertEquals(expected, actual);
    }

    /*@Test the reference is different
    void createArmor_validArmor_shouldReturnArmor() throws InvalidArmorException {
        Armor expected = new Armor("name", 1, "Head", "Cloth", 4);

        Armor actual = Armor.createArmor("name", 1, "Head", "Cloth", 4);

        assertTrue(Objects.deepEquals(expected, actual));
    } */

    @Test
    void createArmor_invalidArmor_shouldReturnNull(){
        Armor expected = null;

        Armor actual = Armor.createArmor("name", 1, "Head", "clo", 4);

        assertEquals(expected, actual);
    }

    @Test
    void setArmorType_invalidArmorType_shouldThrowException(){
        String expected = "Invalid Armor Type!";

        Exception exception = assertThrows(InvalidArmorException.class, ()-> {
           setArmorType("Clo");
        });

        String actual = exception.getMessage();

        assertEquals(expected,actual);

    }


}