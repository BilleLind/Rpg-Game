package Equipment;

import org.junit.jupiter.api.Test;
import Equipment.Weapon.InvalidWeaponException;

import static Equipment.Weapon.createWeapon;
import static Equipment.Weapon.setWeaponType;
import static org.junit.jupiter.api.Assertions.*;

class WeaponTest {

    @Test
    void setWeaponType_invalidWeaponType_shouldThrowException() {

        String expected = "Invalid Weapon Type!";

        Exception exception = assertThrows(InvalidWeaponException.class, ()-> {
            setWeaponType("gun");
        });

        String actual = exception.getMessage();

        assertEquals(expected,actual);

    }

    @Test
    void createWeapon_invalidWeapon_shouldReturnNull() {
        Weapon expected = null;

        Weapon actual = Weapon.createWeapon("name", 1, 10, 1, "Dag");

        assertEquals(expected, actual);

    }

    @Test
    void getDamage() {
        double expected = 10.0;
        Weapon weapon = Weapon.createWeapon("name", 1, 10, 1, "Dagger");

        double actual = weapon.getDamage();

        assertEquals(expected, actual);
    }

    @Test
    void setDamage() {
        int expected = 5;
        Weapon weapon = createWeapon("name", 1, 10, 1, "Dagger");

        weapon.setDamage(5);
        int actual = weapon.getDamage();

        assertEquals(expected, actual);
    }

    @Test
    void getAttackPerSecond() {
        int expected = 1;
        Weapon weapon = createWeapon("name", 1, 10, 1, "Dagger");

        int actual = weapon.getAttackPerSecond();

        assertEquals(expected, actual);
    }

    @Test
    void setAttackPerSecond() {
        int expected = 5;
        Weapon weapon = createWeapon("name", 1, 10, 1, "Dagger");

        weapon.setAttackPerSecond(5);
        int actual = weapon.getAttackPerSecond();

        assertEquals(expected, actual);
    }

    @Test
    void getDPS() {
        int expected = 20;
        Weapon weapon = createWeapon("name", 1, 10, 2, "Dagger");

        double actual = weapon.getDPS();

        assertEquals(expected, actual);
    }
}