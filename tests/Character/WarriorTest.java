package Character;

import Equipment.Armor;
import Equipment.Weapon;
import org.junit.jupiter.api.Test;

import static Equipment.Armor.createArmor;
import static Equipment.Weapon.createWeapon;
import static org.junit.jupiter.api.Assertions.*;

class WarriorTest {

    @Test
    void levelUp() {
        Warrior test = new Warrior("Warrior");
        //start intelligence for Warriors + 5
        int expected = 8;

        test.levelUp();
        int actual = test.getPrimaryAttribute();

        assertEquals(expected, actual);

    }

    @Test
    void equitable_validWarriorWeapon_shouldReturnWeapon(){
        Warrior test = new Warrior("Warrior");
        Weapon weaponForTest = createWeapon("name", 1,3, 1,"Axe");

        test.equipWeapon(weaponForTest);

        assertEquals(weaponForTest, test.manifestWeapon());
    }

    @Test
    void equitable_validWarriorHeadArmor_shouldReturnHeadArmor(){
        Warrior test = new Warrior("Warrior");
        Armor armor = createArmor("name", 1, "Head", "Plate", 4);

        test.equipHead(armor);

        assertEquals(armor, test.manifestArmor("Head"));
    }
    //Could be replicated for Legs and Body.

    @Test
    void equitable_invalidWarriorHeadArmor_shouldReturnNull(){
        Warrior test = new Warrior("Warrior");
        Armor armor = createArmor("name", 1, "Head", "Cloth", 4);
        String expected = null;

        test.equipHead(armor);
        Armor actual = test.manifestArmor("Head");

        assertEquals(expected, actual);
    }

    @Test
    void TotalPrimaryAttributes_noneArmorEquipped_shouldReturnBaseAttributes(){
        Warrior test = new Warrior("Warrior");
        int expected = test.getPrimaryAttribute();

        int actual = test.getTotalPrimaryAttribute();

        assertEquals(expected,actual);
    }

    @Test
    void TotalPrimaryAttributes_1ArmorEquipped_shouldReturnTotalBaseAttributes(){
        Warrior test = new Warrior("Warrior");
        int armorAttribute = 4;
        int expected = test.getPrimaryAttribute() + armorAttribute;
        Armor head = createArmor("name", 1, "Head", "Plate", 4);
        test.equipHead(head);

        int actual = test.getTotalPrimaryAttribute();

        assertEquals(expected,actual);
    }

    //copied from temp hero test
    @Test
    void getPrimaryAttribute_primaryShouldBeLargest() {
        Warrior test = new Warrior("Warrior");
        //start intelligence for Warriors
        int expected = 5;

        int actual = test.getPrimaryAttribute();

        assertEquals(expected, actual);
    }










    @Test
    void equitable_WarriorBodyArmor_shouldReturnBodyArmor() {
        Warrior test = new Warrior("Warrior");
        Armor armor = createArmor("name", 1, "Body", "Plate", 4);

        test.equipBody(armor);

        assertEquals(armor, test.manifestArmor("Body"));
    }

    @Test
    void equitable_WarriorLegsArmor_shouldReturnLegsArmor() {
        Warrior test = new Warrior("Warrior");
        Armor armor = createArmor("name", 1, "Legs", "Plate", 4);

        test.equipLegs(armor);

        assertEquals(armor, test.manifestArmor("Legs"));
    }

    @Test
    void manifestWeapon_noneEquipped_shouldReturnNull() {
        Warrior test = new Warrior("Warrior");
        String expected = null;

        assertEquals(expected, test.manifestWeapon());
    }

    @Test
    void manifestWeapon_validEquipped_shouldReturnWeapon() {
        Warrior test = new Warrior("Warrior");
        Weapon expected = createWeapon("name", 1,3, 1,"Axe");


        test.equipWeapon(expected);

        assertEquals(expected, test.manifestWeapon());
    }


    @Test
    void manifestArmor_noneEquipped_shouldReturnNull() {
        Warrior test = new Warrior("Warrior");
        String expected = null;

        assertEquals(expected, test.manifestArmor("Head"));
    }

    @Test
    void manifestArmor_validEquipped_shouldReturnArmor() {
        Warrior test = new Warrior("Warrior");
        Armor expected = createArmor("name", 1, "Head", "Plate", 4);


        test.equipHead(expected);

        assertEquals(expected, test.manifestArmor("Head"));
    }


    @Test
    void getName() {
        Warrior test = new Warrior("Warrior");
        String expected = "Warrior";

        String actual = test.getName();

        assertEquals(expected, actual);
    }

    @Test
    void calculateDaWarrior_withoutWeapon_ShouldReturnOne() {
        Warrior test = new Warrior("Warrior");
        double expected = 1.05;

        double actual = test.calculateDamage();

        assertEquals(expected, actual);

    }

    @Test
    void calculateDamage_withWeapon_ShouldReturnDouble() {
        Warrior test = new Warrior("Warrior");
        Weapon weapon = createWeapon("name", 1,5, 2,"Axe");
        test.equipWeapon(weapon);
        double expected = 10.5;

        double actual = test.calculateDamage();

        assertEquals(expected, actual);

    }

}