package Character;

import Equipment.Armor;
import Equipment.Weapon;
import org.junit.jupiter.api.Test;

import static Equipment.Armor.createArmor;
import static Equipment.Weapon.createWeapon;
import static org.junit.jupiter.api.Assertions.*;

class RangerTest {
    @Test
    void levelUp() {
        Ranger test = new Ranger("Ranger");
        //start intelligence for Rangers + 5
        int expected = 12;

        test.levelUp();
        int actual = test.getPrimaryAttribute();

        assertEquals(expected, actual);

    }

    @Test
    void equitable_validRangerWeapon_shouldReturnWeapon(){
        Ranger test = new Ranger("Ranger");
        Weapon weaponForTest = createWeapon("name", 1,3, 1,"Bow");

        test.equipWeapon(weaponForTest);

        assertEquals(weaponForTest, test.manifestWeapon());
    }

    @Test
    void equitable_validRangerHeadArmor_shouldReturnHeadArmor(){
        Ranger test = new Ranger("Ranger");
        Armor armor = createArmor("name", 1, "Head", "Mail", 4);

        test.equipHead(armor);

        assertEquals(armor, test.manifestArmor("Head"));
    }
    //Could be replicated for Legs and Body.

    @Test
    void equitable_invalidRangerHeadArmor_shouldReturnNull(){
        Ranger test = new Ranger("Ranger");
        Armor armor = createArmor("name", 1, "Head", "Cloth", 4);
        String expected = null;

        test.equipHead(armor);
        Armor actual = test.manifestArmor("Head");

        assertEquals(expected, actual);
    }

    @Test
    void TotalPrimaryAttributes_noneArmorEquipped_shouldReturnBaseAttributes(){
        Ranger test = new Ranger("Ranger");
        int expected = test.getPrimaryAttribute();

        int actual = test.getTotalPrimaryAttribute();

        assertEquals(expected,actual);
    }

    @Test
    void TotalPrimaryAttributes_1ArmorEquipped_shouldReturnTotalBaseAttributes(){
        Ranger test = new Ranger("Ranger");
        int armorAttribute = 4;
        int expected = test.getPrimaryAttribute() + armorAttribute;
        Armor head = createArmor("name", 1, "Head", "Mail", 4);
        test.equipHead(head);

        int actual = test.getTotalPrimaryAttribute();

        assertEquals(expected,actual);
    }

    //copied from temp hero test
    @Test
    void getPrimaryAttribute_primaryShouldBeLargest() {
        Ranger test = new Ranger("Ranger");
        //start intelligence for Rangers
        int expected = 7;

        int actual = test.getPrimaryAttribute();

        assertEquals(expected, actual);
    }










    @Test
    void equitable_RangerBodyArmor_shouldReturnBodyArmor() {
        Ranger test = new Ranger("Ranger");
        Armor armor = createArmor("name", 1, "Body", "Mail", 4);

        test.equipBody(armor);

        assertEquals(armor, test.manifestArmor("Body"));
    }

    @Test
    void equitable_RangerLegsArmor_shouldReturnLegsArmor() {
        Ranger test = new Ranger("Ranger");
        Armor armor = createArmor("name", 1, "Legs", "Mail", 4);

        test.equipLegs(armor);

        assertEquals(armor, test.manifestArmor("Legs"));
    }

    @Test
    void manifestWeapon_noneEquipped_shouldReturnNull() {
        Ranger test = new Ranger("Ranger");
        String expected = null;

        assertEquals(expected, test.manifestWeapon());
    }

    @Test
    void manifestWeapon_validEquipped_shouldReturnWeapon() {
        Ranger test = new Ranger("Ranger");
        Weapon expected = createWeapon("name", 1,3, 1,"Bow");


        test.equipWeapon(expected);

        assertEquals(expected, test.manifestWeapon());
    }


    @Test
    void manifestArmor_noneEquipped_shouldReturnNull() {
        Ranger test = new Ranger("Ranger");
        String expected = null;

        assertEquals(expected, test.manifestArmor("Head"));
    }

    @Test
    void manifestArmor_validEquipped_shouldReturnArmor() {
        Ranger test = new Ranger("Ranger");
        Armor expected = createArmor("name", 1, "Head", "Mail", 4);


        test.equipHead(expected);

        assertEquals(expected, test.manifestArmor("Head"));
    }


    @Test
    void getName() {
        Ranger test = new Ranger("Ranger");
        String expected = "Ranger";

        String actual = test.getName();

        assertEquals(expected, actual);
    }

    @Test
    void calculateDaRanger_withoutWeapon_ShouldReturnOne() {
        Ranger test = new Ranger("Ranger");
        double expected = 1.07;

        double actual = test.calculateDamage();

        assertEquals(expected, actual);

    }

    @Test
    void calculateDamage_withWeapon_ShouldReturnDouble() {
        Ranger test = new Ranger("Ranger");
        Weapon weapon = createWeapon("name", 1,3, 2,"Bow");
        test.equipWeapon(weapon);
        double expected = 6.42;

        double actual = test.calculateDamage();

        assertEquals(expected, actual);

    }

}