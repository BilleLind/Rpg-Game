package Character;

import Equipment.Armor;
import Equipment.Equipment;
import Equipment.Weapon;
import org.junit.jupiter.api.Test;
import  Equipment.Armor.InvalidArmorException;

import static Equipment.Armor.createArmor;
import static Equipment.Weapon.createWeapon;

import static org.junit.jupiter.api.Assertions.*;

class MageTest {

    @Test
    void levelUp() {
        Mage test = new Mage("mage");
        //start intelligence for mages + 5
        int expected = 13;

        test.levelUp();
        int actual = test.getPrimaryAttribute();

        assertEquals(expected, actual);

    }

    @Test
    void equitable_validMageWeapon_shouldReturnWeapon(){
        Mage test = new Mage("mage");
        Weapon weaponForTest = createWeapon("name", 1,3, 1,"Wand");

        test.equipWeapon(weaponForTest);

        assertEquals(weaponForTest, test.manifestWeapon());
    }

    @Test
    void equitable_validMageHeadArmor_shouldReturnHeadArmor(){
        Mage test = new Mage("mage");
        Armor armor = createArmor("name", 1, "Head", "Cloth", 4);

        test.equipHead(armor);

        assertEquals(armor, test.manifestArmor("Head"));
    }
    //Could be replicated for Legs and Body.

    @Test
    void equitable_invalidMageHeadArmor_shouldReturnNull(){
        Mage test = new Mage("mage");
        Armor armor = createArmor("name", 1, "Head", "Leather", 4);
        String expected = null;

        test.equipHead(armor);
        Armor actual = test.manifestArmor("Head");

        assertEquals(expected, actual);
    }

    @Test
    void TotalPrimaryAttributes_noneArmorEquipped_shouldReturnBaseAttributes(){
        Mage test = new Mage("Mage");
        int expected = test.getPrimaryAttribute();

        int actual = test.getTotalPrimaryAttribute();

        assertEquals(expected,actual);
    }

    @Test
    void TotalPrimaryAttributes_1ArmorEquipped_shouldReturnTotalBaseAttributes(){
        Mage test = new Mage("Mage");
        int armorAttribute = 4;
        int expected = test.getPrimaryAttribute() + armorAttribute;
        Armor head = createArmor("name", 1, "Head", "Cloth", 4);
        test.equipHead(head);

        int actual = test.getTotalPrimaryAttribute();

        assertEquals(expected,actual);
    }

    //copied from temp hero test
    @Test
    void getPrimaryAttribute_primaryShouldBeLargest() {
        Mage test = new Mage("mage");
        //start intelligence for mages
        int expected = 8;

        int actual = test.getPrimaryAttribute();

        assertEquals(expected, actual);
    }










    @Test
    void equitable_mageBodyArmor_shouldReturnBodyArmor() {
        Mage test = new Mage("mage");
        Armor armor = createArmor("name", 1, "Body", "Cloth", 4);

        test.equipBody(armor);

        assertEquals(armor, test.manifestArmor("Body"));
    }

    @Test
    void equitable_mageLegsArmor_shouldReturnLegsArmor() {
        Mage test = new Mage("mage");
        Armor armor = createArmor("name", 1, "Legs", "Cloth", 4);

        test.equipLegs(armor);

        assertEquals(armor, test.manifestArmor("Legs"));
    }

    @Test
    void manifestWeapon_noneEquipped_shouldReturnNull() {
        Mage test = new Mage("mage");
        String expected = null;

        assertEquals(expected, test.manifestWeapon());
    }

    @Test
    void manifestWeapon_validEquipped_shouldReturnWeapon() {
        Mage test = new Mage("mage");
        Weapon expected = createWeapon("name", 1,3, 1,"Wand");


        test.equipWeapon(expected);

        assertEquals(expected, test.manifestWeapon());
    }


    @Test
    void manifestArmor_noneEquipped_shouldReturnNull() {
        Mage test = new Mage("mage");
        String expected = null;

        assertEquals(expected, test.manifestArmor("Head"));
    }

    @Test
    void manifestArmor_validEquipped_shouldReturnArmor() {
        Mage test = new Mage("mage");
        Armor expected = createArmor("name", 1, "Head", "Cloth", 4);


        test.equipHead(expected);

        assertEquals(expected, test.manifestArmor("Head"));
    }


    @Test
    void getName() {
        Mage test = new Mage("mage");
        String expected = "mage";

        String actual = test.getName();

        assertEquals(expected, actual);
    }

    @Test
    void calculateDamage_withoutWeapon_ShouldReturnOne() {
        Mage test = new Mage("mage");
        double expected = 1.08;

        double actual = test.calculateDamage();

        assertEquals(expected, actual);

    }

    @Test
    void calculateDamage_withWeapon_ShouldReturnDouble() {
        Mage test = new Mage("mage");
        Weapon weapon = createWeapon("name", 1,3, 2,"Wand");
        test.equipWeapon(weapon);
        double expected = 6.48;

        double actual = test.calculateDamage();

        assertEquals(expected, actual);

    }

}