package Character;

import Equipment.Armor;
import Equipment.Weapon;
import org.junit.jupiter.api.Test;

import static Equipment.Armor.createArmor;
import static Equipment.Weapon.createWeapon;
import static org.junit.jupiter.api.Assertions.*;

class RogueTest {


    @Test
    void levelUp() {
        Rogue test = new Rogue("Rogue");
        //start intelligence for Rogues + 4
        int expected = 10;

        test.levelUp();
        int actual = test.getPrimaryAttribute();

        assertEquals(expected, actual);

    }

    @Test
    void equitable_validRogueWeapon_shouldReturnWeapon(){
        Rogue test = new Rogue("Rogue");
        Weapon weaponForTest = createWeapon("name", 1,3, 1,"Dagger");

        test.equipWeapon(weaponForTest);

        assertEquals(weaponForTest, test.manifestWeapon());
    }

    @Test
    void equitable_validRogueHeadArmor_shouldReturnHeadArmor(){
        Rogue test = new Rogue("Rogue");
        Armor armor = createArmor("name", 1, "Head", "Leather", 4);

        test.equipHead(armor);

        assertEquals(armor, test.manifestArmor("Head"));
    }
    //Could be replicated for Legs and Body.

    @Test
    void equitable_invalidRogueHeadArmor_shouldReturnNull(){
        Rogue test = new Rogue("Rogue");
        Armor armor = createArmor("name", 1, "Head", "Cloth", 4);
        String expected = null;

        test.equipHead(armor);
        Armor actual = test.manifestArmor("Head");

        assertEquals(expected, actual);
    }

    @Test
    void TotalPrimaryAttributes_noneArmorEquipped_shouldReturnBaseAttributes(){
        Rogue test = new Rogue("Rogue");
        int expected = test.getPrimaryAttribute();

        int actual = test.getTotalPrimaryAttribute();

        assertEquals(expected,actual);
    }

    @Test
    void TotalPrimaryAttributes_1ArmorEquipped_shouldReturnTotalBaseAttributes(){
        Rogue test = new Rogue("Rogue");
        int armorAttribute = 4;
        int expected = test.getPrimaryAttribute() + armorAttribute;
        Armor head = createArmor("name", 1, "Head", "Leather", 4);
        test.equipHead(head);

        int actual = test.getTotalPrimaryAttribute();

        assertEquals(expected,actual);
    }

    //copied from temp hero test
    @Test
    void getPrimaryAttribute_primaryShouldBeLargest() {
        Rogue test = new Rogue("Rogue");
        //start intelligence for Rogues
        int expected = 6;

        int actual = test.getPrimaryAttribute();

        assertEquals(expected, actual);
    }










    @Test
    void equitable_RogueBodyArmor_shouldReturnBodyArmor() {
        Rogue test = new Rogue("Rogue");
        Armor armor = createArmor("name", 1, "Body", "Leather", 4);

        test.equipBody(armor);

        assertEquals(armor, test.manifestArmor("Body"));
    }

    @Test
    void equitable_RogueLegsArmor_shouldReturnLegsArmor() {
        Rogue test = new Rogue("Rogue");
        Armor armor = createArmor("name", 1, "Legs", "Leather", 4);

        test.equipLegs(armor);

        assertEquals(armor, test.manifestArmor("Legs"));
    }

    @Test
    void manifestWeapon_noneEquipped_shouldReturnNull() {
        Rogue test = new Rogue("Rogue");
        String expected = null;

        assertEquals(expected, test.manifestWeapon());
    }

    @Test
    void manifestWeapon_validEquipped_shouldReturnWeapon() {
        Rogue test = new Rogue("Rogue");
        Weapon expected = createWeapon("name", 1,3, 1,"Dagger");


        test.equipWeapon(expected);

        assertEquals(expected, test.manifestWeapon());
    }


    @Test
    void manifestArmor_noneEquipped_shouldReturnNull() {
        Rogue test = new Rogue("Rogue");
        String expected = null;

        assertEquals(expected, test.manifestArmor("Head"));
    }

    @Test
    void manifestArmor_validEquipped_shouldReturnArmor() {
        Rogue test = new Rogue("Rogue");
        Armor expected = createArmor("name", 1, "Head", "Leather", 4);


        test.equipHead(expected);

        assertEquals(expected, test.manifestArmor("Head"));
    }


    @Test
    void getName() {
        Rogue test = new Rogue("Rogue");
        String expected = "Rogue";

        String actual = test.getName();

        assertEquals(expected, actual);
    }

    @Test
    void calculateDaRogue_withoutWeapon_ShouldReturnOne() {
        Rogue test = new Rogue("Rogue");
        double expected = 1.06;

        double actual = test.calculateDamage();

        assertEquals(expected, actual);

    }

    @Test
    void calculateDamage_withWeapon_ShouldReturnDouble() {
        Rogue test = new Rogue("Rogue");
        Weapon weapon = createWeapon("name", 1,3, 2,"Dagger");
        test.equipWeapon(weapon);
        double expected = 6.36;

        double actual = test.calculateDamage();

        assertEquals(expected, actual);

    }

}