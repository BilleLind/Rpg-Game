
# RPG console Game created with Java.

This project is to both practice and show the usage of OOP and JUnit testing, through the creation of the basics for a Console Log Game.

Where the focus were on The Game Character (Hero) and the Avaiable classes(Mage, Hunter, Warrior, Rogue), along with the ability to have Equipment (Weapon And Armor)
to further increase ones damage.



## Installation
    Requirements: 
        - JDK 17:
        - Git

#### Intellij:
        1. Create new Project from version control
        2. Choose the following as source:
            https://gitlab.com/BilleLind/Rpg-Game.git
        2A. Select the wanted folder
        3. Click Clone.
#### Command Line:
        1. Move into or create the folder that should contain the Projects files. eg. /programming/
        2. git clone https://gitlab.com/BilleLind/Rpg-Game.git
        3. cd Rpg-Game - move into game folder
        4. javac -d out src/*.java src/rpg/*.java
        5. cd into rpg => cd out/src/rpg
        6. java Main
 

## Running Tests (Intellij)

    Right click on the tests folder and select " Run 'All Tests' "


## Usage/Examples
In the Main.java, located /src/Main.java there are an example of the game functionality implemented.

### Hero
```java
int strength =1, dexterity=1, intelligence= 8;

Mage mage = new Mage("name", strength, dexterity, intelligence);

// Weapon and Armor are extended by Equipment which is the abstract that creates the HashMap used for the Hero Armor and Weapon management.              
Weapon wand = createWeapon("Tree Branch", 1, 4, 1, "Wand"); // String name, int requiredLevel, int damage, int attackPerSecond, String type

// Each of the Armor types follow the equipWeapon's approach, eg. equipHead, EquipBody, EquipLegs.
mage.equipWeapon(wand);

//since when equipping Weapon/Armor it is type casted to Equipment, one would need to cast it back in order to gain access to Armor or Weapon specific methods.
//therefore i created .manifestWeapon and manifestArmor(SLOT); which simplifies the process:

//From 2 needing 2 classes, one for getting and the other for casting to only one.
Weapon equippedWeapon = mage.manifestWeapon();

//Which the following methods also uses internally

double damageDealt = mage.calculateDamage();

int TotalPrimaryAttributes = mage.equipmentAttributes();


```

